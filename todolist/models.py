from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ToDoItem(models.Model):
    # CharField = String value
    # max_length - > is a modifier
    task_name = models.CharField(max_length=50)    
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Pending")
    # DateTimeField = DateTime datatype
    date_created = models.DateTimeField("Date Created")
    # Adding a user_id to the ToDoItem table, which is a foreign key connected to the Users table.
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")

class EventItem(models.Model):
    # CharField = String value
    # max_length - > is a modifier
    event_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Pending")
    # DateTimeField = DateTime datatype
    date_created = models.DateTimeField("Date Created")
    # Adding a user_id to the ToDoItem table, which is a foreign key connected to the Users table.
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")
